/*    Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include "BinExpr.hpp"
#include <iostream>

BinExpr::BinExpr(Expr* a1, Expr* a2): m_a1(a1), m_a2(a2) {}
AddExpr::AddExpr(Expr* n1, Expr* n2) : BinExpr(n1, n2) {}
SubExpr::SubExpr(Expr* n1, Expr* n2) : BinExpr(n1, n2) {}
MulExpr::MulExpr(Expr* n1, Expr* n2) : BinExpr(n1, n2) {}
DivExpr::DivExpr(Expr* n1, Expr* n2) : BinExpr(n1, n2) {}

Number AddExpr::eval() const
{
    return left()->eval() + right()->eval();
}

Number SubExpr::eval() const
{
    return left()->eval() - right()->eval();
}

Number MulExpr::eval() const
{
    return left()->eval() * right()->eval();
}

Number DivExpr::eval() const
{
    return left()->eval() / right()->eval();
}
