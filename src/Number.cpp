/*    Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef NUMBER_HPP
#define NUMBER_HPP

#include "Number.hpp"
#include "Exception.hpp"

Number::Number(long val)
{
    m_val.l = val;
    m_type = T_INTEGER;
}

Number::Number(double val)
{
    m_val.d = val;
    m_type = T_DOUBLE;
}

#define GET_NUMBER_VAL(n)    (((n).m_type == T_INTEGER)? (n).m_val.l : (n).m_val.d)
#define PERFORM_OPERATION(op)   ( GET_NUMBER_VAL(*this) op GET_NUMBER_VAL(n) )

Number Number::operator+(const Number& n) const {
    return PERFORM_OPERATION(+);
}

Number Number::operator-(const Number& n) const {
    return PERFORM_OPERATION(-);
}

Number Number::operator*(const Number& n) const {
    return PERFORM_OPERATION(*);
}

Number Number::operator/(const Number& n) const {
    if (GET_NUMBER_VAL(n) == 0) throw DivByZero("");
    return PERFORM_OPERATION(/);
}

Number Number::eval() const {
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Number& n) {
    std::cout << GET_NUMBER_VAL(n);
}

#endif
