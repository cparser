/*    Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef __BINEXPR_HPP
#define __BINEXPR_HPP

#include "Expr.hpp"
#include "Number.hpp"

class BinExpr : public Expr
{
public:
    BinExpr(Expr* a1, Expr* a2);
protected:
    Expr* left() const { return m_a1; }
    Expr* right() const { return m_a2; }
private:
    Expr* m_a1;
    Expr* m_a2;
};

class AddExpr : public BinExpr
{
public:
    AddExpr(Expr* n1, Expr* n2);
    Number eval() const;
};

class SubExpr : public BinExpr
{
public:
    SubExpr(Expr* n1, Expr* n2);
    Number eval() const;
};

class MulExpr : public BinExpr
{
public:
    MulExpr(Expr* n1, Expr* n2);
    Number eval() const;
};

class DivExpr : public BinExpr
{
public:
    DivExpr(Expr* n1, Expr* n2);
    Number eval() const;
};

#endif /* __BINEXPR.HPP */
